TestIterator:
Also try with a LinkedList - does it make any difference behaviorally? (ignore performance)
 It does not make a behavioral difference.
What happens if you use list.remove(Integer.valueOf(77))?
 An error is thrown because we affected the list directly, therefore, the iterator has no value to jump back to since it cannot see this change.

TestList:
Also try with a LinkedList - does it make any difference behaviorally? (ignore performance)
 It does not make a behavioral difference.
list.remove(5); // answer: what does this method do?
 It removes the object at index 5 in the list
list.remove(Integer.valueOf(5)); // answer: what does this one do?
 It removes the value 5 that's in the list

 TestPerformance:
 TEST RESULTS: (time is in ms)
 Size   10      100     1,000     10,000     100,000
 AAC    9ms     9ms     10ms       12ms       18ms
 LAR    19ms    20ms    20ms       22ms       28ms
 LAC    7ms     20ms    292ms      3,654ms    41,932ms
 AAR    18ms    27ms    122ms      962ms      16,529ms
 Answer: which of the two lists performs better for each method as the size increases?
 ArrayList performs better